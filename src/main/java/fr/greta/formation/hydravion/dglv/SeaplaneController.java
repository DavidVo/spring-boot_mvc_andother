package fr.greta.formation.hydravion.dglv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SeaplaneController {
	
	private final Logger log= LoggerFactory.getLogger(SeaplaneController.class);
	
	@RequestMapping("/Seaplanes")
	public String show(Model mod) {
		
		log.info("Show seaplanes");
		
		mod.addAttribute("l", source.getList());
		
		return "seaplanes";
	}
	
	// Ici de private a setSoucre + @Service au debut de la classe SeaplaneSource
	// permet d'injecter SeaplaneSource dans SeaPlaneControler
	private SeaplaneSource source;
	
	public SeaplaneSource getSource() {
		return source;
	}
	@Autowired
	public void setSource(SeaplaneSource source) {
		this.source = source;
	}
	
}
