package fr.greta.formation.hydravion.dglv;

public class Seaplane {
	private boolean outshore;
	private String model;
	private String Rego;
	
	public boolean isOutshore() {
		return outshore;
	}
	public void setOutshore(boolean outshore) {
		this.outshore = outshore;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRego() {
		return Rego;
	}
	public void setRego(String rego) {
		Rego = rego;
	}
	
	public Seaplane(boolean bo, String mo, String re) {
		outshore=bo;
		model=mo;
		Rego=re;
	}
	
	@Override
	public String toString() {
		return "Seaplane [outshore=" + outshore + ", model=" + model + ", Rego=" + Rego + "]";
	}
	
	
}
