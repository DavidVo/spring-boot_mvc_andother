package fr.greta.formation.hydravion.dglv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//Du au schema dans mon livret cette methode doit être placée dans le meme package que Application
//Pour les liens voir schéma dans livret 

@Controller
public class NewsController {
	
	private final Logger log= LoggerFactory.getLogger(NewsController.class);
	
	// Cette annotation suivant sert à faire le lien "URL to controller"
	@RequestMapping("/news")
	// Creation du lien "controller to template" avec la ligne suivante.
	// Ceci est permis car nous avons utilisé les thermes normé de la dépendance "...-starter-thymeleaf"
	public String show(Model mod) {
		// Permet de choisir l'importanc des messages que l'on veut logger
		log.info("Show news " + newsTitle);
		mod.addAttribute("title", newsTitle);
		mod.addAttribute("text", newsText);
		
		return "news";

	}
	
	@Value("${news.title}")
	private String newsTitle;
	@Value("${news.text}")
	private String newsText;
	
}
