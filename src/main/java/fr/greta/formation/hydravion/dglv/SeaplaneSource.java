package fr.greta.formation.hydravion.dglv;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service("SeaplaneSource")
public class SeaplaneSource {
	
	public List<Seaplane> getList() {
		List<Seaplane> hydraList = new ArrayList<Seaplane>();
		hydraList.add(new Seaplane(false,"Le Vol bien", "AZ25T"));
		hydraList.add(new Seaplane(true,"Le flotte bien", "BE85C"));
		hydraList.add(new Seaplane(false,"Le pourrait mieux flotter", "AX98TG"));
		hydraList.add(new Seaplane(false,"Le Devrait mieux flotter", "TR32PO"));
		hydraList.add(new Seaplane(true,"Le vol et flotte bien", "DVG87"));
		
		return(hydraList);
	}
}
