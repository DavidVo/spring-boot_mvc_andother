package fr.greta.formation.hydravion.dglv;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {
	
	public static void main(String[] arg0) {
		
		SpringApplication.run(Application.class, arg0);
		
		/**
		LocalDateTime dateNow = LocalDateTime.now(); 
		DateTimeFormatter formatterJour = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter formatterHeure = DateTimeFormatter.ofPattern("HH:mm");
		
		String jourFormatted = dateNow.format(formatterJour);
		String heureFormatted = dateNow.format(formatterHeure);
		
		System.out.println("WELCOME, today is " + jourFormatted + " and it\'s " + heureFormatted);
		 
		System.out.println("Welcome in our seaplane base. It's currently "+ heureFormatted + "and we are the " + jourFormatted);
		**/
	}

}
